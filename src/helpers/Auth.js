import store from '../store/index.js'

function requireAuth (to, from, next)
{
  let jwt   = sessionStorage.getItem("token")
  let user  = JSON.parse(sessionStorage.getItem("user"))
  let empresaSelecionada = JSON.parse(sessionStorage.getItem("empresaSelecionada")) || {}
  store.modules.system.state.token = jwt
  store.modules.system.state.user  = user
  store.modules.system.state.empresaSelecionada = empresaSelecionada
  if( !jwt ) return next("/login")
  next()
}
export default requireAuth
