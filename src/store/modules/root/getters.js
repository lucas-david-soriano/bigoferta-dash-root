export default { 
    getUsuarios: state => state.usuarios,
    getUsuariosTotal: state => state.usuarios.length
}