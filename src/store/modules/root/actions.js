export default {
  listaUsuarios({commit, getters})
	{
      return new Promise( (res,rej) => {
        getters.getApi.get(`usuario`).then( resp => {
          commit('SET_USUARIOS', resp.data)
          res(resp.data)
        })
      })
  },

  cadastraUsuarioRoot({commit, getters}, usuario)
	{
      return new Promise( (res,rej) => {
        getters.getApi.post(`criaroot`, usuario).then( resp => {
          res(resp.data)
        })
      })
  },

  alteraUsuario({commit, getters}, usuario)
	{
      return new Promise( (res,rej) => {
        getters.getApi.put(`usuario`, usuario).then( resp => {
          res(resp.data)
        })
      })
  },

  removeUsuario({commit, getters}, usuario)
	{
        return new Promise( (res,rej) => {
          getters.getApi.delete(`usuario/${usuario._id}`).then( resp => {
            res(resp.data)
          })
        })
  },
}
