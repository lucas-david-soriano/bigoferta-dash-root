export default {
	'SET_USER' (state, u) {
		state.user = u
		sessionStorage.setItem("user", JSON.stringify(u))
  },

	'SET_TOKEN' (state, token) {
    //state.token = {headers:{ Authorization: token }}
    state.token = token
		sessionStorage.setItem("token", token)
  },

	'SET_EMPRESAS' (state, e) {
		state.empresas = e
  },

	'SELECT_EMPRESA' (state, e) {
    sessionStorage.setItem("empresaSelecionada", JSON.stringify(e))
		state.empresaSelecionada = e
  },

  'SET_ISLOADING_CHECK_USER' (state, data) {
    state.isLoadingCheckUser = data
  },

  'SET_DATAUSER' (state, data) {
    state.dataUser = data
  }
}
