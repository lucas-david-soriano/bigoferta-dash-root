export default {
    getUser: state => {return state.user},
    getToken: state => {return state.token},
    getApi: state => {return state.$api},
    getIsLoadingCheckUser: state => {return state.isLoadingCheckUser},
    getDataUser: state => {return state.dataUser},
    getEmpresaSelecionada: state => {
      return Object.keys(state.empresaSelecionada).length > 0 ? state.dataUser.empresas[0] : JSON.parse(window.sessionStorage.getItem("empresaSelecionada"))
    }
}
