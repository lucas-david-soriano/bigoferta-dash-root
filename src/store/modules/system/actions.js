import axios from 'axios'

export default {
	loggout(context)
	{
    sessionStorage.removeItem("token")
  },
  loginDash({commit, getters}, usuario) {
    return new Promise ( (res, rej) => {
      let u = getters.getApi.post('login', usuario)
      res(u)
    })
  },
  setToken ({commit,getters}, data) {
    commit('SET_TOKEN', data.token)

    //commit('SET_EMPRESAS', data.user.estabelecimentos)
    commit('SET_USER', data.user)
    //data.user.estabelecimentos.length > 0 ? commit('SELECT_EMPRESA', data.user.estabelecimentos[0]) : commit('SELECT_EMPRESA', {nome:'Nenhuma empresa selecionada'})
  },

  setEmpresa (context, emp) {
    context.commit('SELECT_EMPRESA', emp)
  },

  checkUsuario ({getters, commit}) {
    getters.getApi.get(`usuario/check` ).then( resp => {
      let dataUser = resp.data.data
      commit('SET_DATAUSER', dataUser )
      commit('SET_ISLOADING_CHECK_USER', false)

      //
		})
  }
}
