export default {
  'SET_ESTADO' (state, data) {
    state.estados = data
  },
  'SET_CIDADE' (state, data) {
    state.cidades = data
  }
}
