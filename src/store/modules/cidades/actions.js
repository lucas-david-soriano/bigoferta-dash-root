export default {

  listaEstado({commit, getters, rootState})
	{
      return new Promise( (res,rej) => {
        getters.getApi.get(`estado`).then( resp => {
          commit('SET_ESTADO', resp.data.data)
          res(resp.data)
        })
      })
  },

  cadastroEstado ({getters, commit, rootState}, data) 
  {
    return new Promise( (res,rej) => {
      getters.getApi.post(`estado`, data).then( resp => {
        res(resp.data)
      })
    })
  },

  alteraEstado({commit, getters, rootState}, data)
	{
    return new Promise( (res,rej) => {
        getters.getApi.put(`estado`, data).then( resp => {
          res(resp.data)
        })
    })
  },

  deletaEstado({commit, getters, rootState}, estado)
	{
    return new Promise( (res,rej) => {
          getters.getApi.delete(`estado/${estado._id}`).then( resp => {
            res(resp.data)
          })
    })
  },

  listaCidade({commit, getters, rootState})
	{
    return new Promise( (res,rej) => {
        getters.getApi.get(`cidade`).then( resp => {
          commit('SET_CIDADE', resp.data.data)
          res(resp.data)
        })
    })
  },

  cadastroCidade ({getters, commit, rootState}, data) 
  {
    return new Promise( (res,rej) => {
      getters.getApi.post(`cidade`, data).then( resp => {
        res(resp.data)
      })
    })
  },

  alteraCidade({commit, getters, rootState}, data)
	{
    return new Promise( (res,rej) => {
        getters.getApi.put(`cidade`, data).then( resp => {
          res(resp.data)
        })
    })
  },

  deletaCidade({commit, getters, rootState}, cidade)
	{
    return new Promise( (res,rej) => {
          getters.getApi.delete(`cidade/${cidade._id}`).then( resp => {
            res(resp.data)
          })
    })
  },
}
