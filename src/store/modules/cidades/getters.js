export default {
  getEstados: state => state.estados,
  getCidades: state => state.cidades,
  getCidadesTotal: state => state.cidades.length,
  getCidadesEstado: state => {
    let estados = state.cidades.map( f => f.estado.nome)
    let unique = []

    //função para array de valores unicos
    estados.forEach( ch => {  if( !unique.find( ff => ch == ff ) ){unique.push(ch)} } )

    return unique.map(c => {
      return [ c, estados.filter(f => f == c).length ]
    })
    
  }
}
