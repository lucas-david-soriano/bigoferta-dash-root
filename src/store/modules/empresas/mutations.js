export default {
  'SET_EMPRESAS' (state, data) {
    state.empresas = data
  },
  'SET_CATEGORIAS' (state, data) {
    state.categorias = data
  }
}
