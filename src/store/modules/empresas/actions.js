export default {


  listaEmpresa({commit, getters, rootState})
	{
    return new Promise( (res,rej) => {
          getters.getApi.get(`empresa`).then( resp => {
            commit('SET_EMPRESAS', resp.data)
            res(resp.data)
          })
    })
  },

  alteraEmpresa({commit, getters, rootState}, data)
	{
    return new Promise( (res,rej) => {
        getters.getApi.put(`empresa`, data).then( resp => {
          res(resp.data)
        })
    })
  },

  deletaEmpresa({commit, getters, rootState}, data)
	{       
    return new Promise( (res,rej) => {
        getters.getApi.delete(`empresa/${data}`).then( resp => {
          res(resp.data)
        })
    })
  },

  listaCategoriaEmpresa ({getters, commit, rootState}, data) {
  
    return new Promise( (res,rej) => {
      getters.getApi.get(`categoriaempresa`, data).then( resp => {
        commit('SET_CATEGORIAS', resp.data.data)
        res(resp.data)
      })
    })
  },

  cadastroCategoria ({getters, commit, rootState}, data) {
    console.log(data)
    return new Promise( (res,rej) => {
      getters.getApi.post(`categoriaempresa`, data).then( resp => {
        res(resp.data)
      })
    })
  },

  alteraCategoria ({getters, commit, rootState}, data) {
    console.log(data)
    return new Promise( (res,rej) => {
      getters.getApi.put(`categoriaempresa`, data).then( resp => {
        res(resp.data)
      })
    })
  },


  cadastroEmpresas ({getters, commit, rootState}, data) {
    console.log(data)

    


    return new Promise( (res,rej) => {
      getters.getApi.post(`empresa`, data).then( resp => {
        res(resp.data)
      })
    })
  }
}
