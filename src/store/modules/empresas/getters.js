export default {
  getEmpresas: state => state.empresas,
  getCategorias: state=> state.categorias,
  getEmpresasTotal: state => state.empresas.length,
}
