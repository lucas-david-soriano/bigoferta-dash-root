export default {
  getProdutos: state => state.produtos,
  getCategoriasProdutos: state => state.categorias,
  getProdutosTotal: state => state.produtos.length,
  getProdutosCategoria: state => {
    let categorias = state.produtos.map( f => f.categoria.nome)
    let unique = []

    //função para array de valores unicos
    categorias.forEach( ch => {  if( !unique.find( ff => ch == ff ) ){unique.push(ch)} } )

    return unique.map(c => {
      return [ c, categorias.filter(f => f == c).length ]
    })
    
  },
  getProdutosEmpresa: state => {
    let empresas = state.produtos.map( f => f.empresa.nome)
    let unique = []

    //função para array de valores unicos
    empresas.forEach( ch => {  if( !unique.find( ff => ch == ff ) ){unique.push(ch)} } )

    return unique.map(c => {
      return [ c, empresas.filter(f => f == c).length ]
    })
    
  }
}
