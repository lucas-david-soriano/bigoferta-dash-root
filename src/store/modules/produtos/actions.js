export default {
  loadProdutos ({getters,commit})
  {

    return new Promise( (res,rej) => {
      getters.getApi.get(`produtos`).then( resp => {
        console.log('produtos', resp.data)
        commit('SET_PRODUTOS', resp.data )
        res(resp.data.status)
      })
    })
  },

  saveProdutos ({getters,commit}, produto)
  {
    /*for(var pair of produto.entries()) {
      console.log(pair[0]+ ', '+ pair[1]);
    }*/

    return new Promise( (res,rej) => {
      rootState.system.$api.post(`produto`, produto).then( resp => {
        res(resp.data)
      })
    })
  },

  editaProduto ({getters,commit}, produto) {
    return new Promise( (res,rej) => {
      getters.getApi.put(`produto`, produto).then( resp => {
        res(resp.data)
      }).catch(e => {
        rej(e)
      })
    })
  },


  listaCategoriaProduto ({getters, commit}) 
  {
    return new Promise( (res, rej) => {
      getters.getApi.get(`categoriaproduto`, ).then( resp => {
        commit('SET_CATEGORIAPRODUTO', resp.data.data)
        res(resp.data)
      })
    })
  },


  cadastraCategoriaProduto({getters, commit}, categoria)
  {
    return new Promise( (res, rej) => {
      getters.getApi.post(`categoriaproduto`, categoria).then( resp => {
        res(resp.data)
      })
    })
  },

  alteraCategoriaProduto({getters, commit}, categoria)
  {
    return new Promise( (res, rej) => {
      getters.getApi.put(`categoriaproduto`, categoria).then( resp => {
        res(resp.data)
      })
    })
  },

  removeCategoriaProduto({getters, commit}, categoria_id)
  {
    return new Promise( (res, rej) => {
      getters.getApi.delete(`categoriaproduto/${categoria_id}`).then( resp => {
        res(resp.data)
      })
    })
  }
}
