import Vue from 'vue'
import Router from 'vue-router'
import Auth from '../helpers/Auth'
import VerifyToken from '../helpers/VerifyToken'
import Login from '../components/Login'
import Home from '../components/Home'

//import ChatRoot from '../components/user/ChatRoot'
//import Chat from '../components/user/chat/Index'

//import MeusDados from '../components/user/MeusDados'
//import Ofertas from '../components/user/Ofertas'
//import Indicadores from '../components/user/Indicadores'

//import Pedidos from '../components/user/pedidos/Pedidos'
//import PedidosAtivos from '../components/user/pedidos/PedidosAtivos'
//import PedidosArquivados from '../components/user/pedidos/PedidosArquivados'

import Produtos from '../components/user/produtos/Produtos'

import Empresas from '../components/user/empresas/Index'

import Index from '../components/user/painelroot/Index'

import Usuarios from '../components/user/usuarios/Usuarios.vue'

import CidadeEstado from '../components/user/cidade-estado/CidadeEstado.vue'
import Cidades from '../components/user/cidade-estado/Cidades'
import Estados from '../components/user/cidade-estado/Estados'
/*import Index from '../components/user/painelroot/Index'
import EmpresasRoot from '../components/user/painelroot/EmpresasRoot'
import UsuariosRoot from '../components/user/painelroot/UsuariosRoot'*/

import Error404 from '../components/Error404'
Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
        path: '/',
        redirect:"/login"
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        //beforeEnter:VerifyToken
    },
    {
        path: '/home',
        name: 'home',
        component: Home,
        beforeEnter:Auth
    },
    {
        path: '/indexroot',
        name: 'indexroot',
        component: Index,
    },
    {
      path: '/empresas',
      name: 'empresas',
      component: Empresas,
     // beforeEnter:Auth
  },
    {
        path: '/produtos',
        name: 'produtos',
        component: Produtos,
        beforeEnter:Auth
    },
    {
        path: '/usuarios',
        name: 'usuarios',
        component: Usuarios,
        beforeEnter:Auth
    },

    {
        path: '/cidade-estado',
        name: 'cidade-estado',
        component: CidadeEstado,
        redirect:'/gerenciacidades',
        beforeEnter:Auth,
        children:[
            {
                path: '/gerenciacidades',
                name: 'gerenciacidades',
                component: Cidades,
                beforeEnter:Auth
            },
            {
                path: '/gerenciaestados',
                name: 'gerenciaestados',
                component: Estados,
                beforeEnter:Auth
            }
        ]
    },
    {
        path: '*',
        name: '404',
        component: Error404
    }
  ]
})
